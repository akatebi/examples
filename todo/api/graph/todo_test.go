package graph

import (
	// "encoding/json"

	"encoding/json"
	"log"
	"testing"

	"bitbucket.org/akatebi/examples/todo/graph/generated"
	"bitbucket.org/akatebi/examples/todo/graph/model"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/introspection"
	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/require"
)

func TestGraph(t *testing.T) {
	c := client.New(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: Initialize()})))

	t.Run("introspection", func(t *testing.T) {
		// Make sure we can run the graphiql introspection query without errors
		var resp interface{}
		c.MustPost(introspection.Query, &resp)
		// log.Printf("### %#v", resp)
	})

	t.Run("User", func(t *testing.T) {
		request := `{
				user(id: "me@gmail.com") {
					id
					userId
					totalCount
					completedCount
					todos {
					  edges {
						node {
						  text
						  complete
						}
					  }
					}
				}
			  }
			  `
		var resp struct{ model.User }
		c.MustPost(request, &resp)
		// log.Printf("### %#v", resp)
		// b, err := json.MarshalIndent(resp, "", "  ")
		// log.Println(string(b), err)
		requireUser(t, resp.User)
	})

	t.Run("Node...User", func(t *testing.T) {
		request := `{
				node(id: "VXNlcjptZUBnbWFpbC5jb20=") {
				  ... on User {
					id
					userId
					totalCount
					completedCount
					todos {
					  pageInfo {
						hasNextPage
						endCursor
					  }
					  edges {
						cursor
						node {
						  text
						  complete
						}
					  }
					}
				  }
				}
			  }
			  `
		var resp struct{ Node model.User }
		c.MustPost(request, &resp)
		// log.Printf("==>> %#v", resp)
		requireUser(t, resp.Node)
	})

	t.Run("Node...Todo", func(t *testing.T) {
		request := `{
			node(id: "VG9kbzox") {
			  ... on Todo {
			   id
				text
				complete
			  }
			}
		  }
		  `
		var resp struct{ Node model.Todo }
		c.MustPost(request, &resp)

		require.Equal(t, "VG9kbzox", resp.Node.ID)
		require.Equal(t, "Taste JavaScript", resp.Node.Text)
		require.Equal(t, true, resp.Node.Complete)
	})

	t.Run("AddTodo", func(t *testing.T) {
		request := `mutation {
			addTodo(input: {text: "Buy Eggs", userId:"me@gmail.com",  clientMutationId: "888"}) {
				clientMutationId
				user {
				id
				userId
				totalCount
				completedCount
				todos {
				  pageInfo {
					hasNextPage
					hasPreviousPage
					startCursor
					endCursor
				  }
				}
			  }
			  todoEdge {
				cursor
				node {
				  id
				  text
				  complete
				}
			  }
			}
		  }
		`
		var resp struct {
			AddTodo model.AddTodoPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
		// UPDATE_SNAPSHOTS=true go test -v -run TestGraph/Add
	})

	t.Run("ChangeTodoStatus", func(t *testing.T) {
		request := `mutation {
			changeTodoStatus(input: {complete: true, userId: "me@gmail.com", id: "VG9kbzox", clientMutationId:"87655"}){
			  todo {
				id
				text
				complete
			  }
			  user {
				id
				userId
				completedCount
				totalCount
			  }
			  clientMutationId
			}
		  }`
		var resp struct {
			ChangeTodoStatus model.ChangeTodoStatusPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
	})

	t.Run("MarkAllTodos", func(t *testing.T) {
		request := `mutation {
			markAllTodos (
				input: { userId: "me@gmail.com",  complete: true, clientMutationId: "888" }
			  ) {
				changedTodos {
				  id
				  text
				  complete
				}
				user {
					id
					userId
					completedCount
					totalCount
				}
				clientMutationId
			  }
		  }`
		var resp struct {
			MarkAllTodos model.MarkAllTodosPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
	})

	t.Run("RemoveCompletedTodos", func(t *testing.T) {
		request := `mutation {
			changeTodoStatus(input: {complete: true, userId: "me@gmail.com", id: "VG9kbzox", clientMutationId:"87655"}){
			  todo {
				id
				text
				complete
			  }
			  user {
				id
				userId
				completedCount
				totalCount
			  }
			  clientMutationId
			}
		  }`
		var resp struct {
			ChangeTodoStatus model.ChangeTodoStatusPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
	})

	t.Run("RenameTodo", func(t *testing.T) {
		request := `mutation {
			renameTodo (
				input: { id: "VG9kbzoy", text:"Learn Go", clientMutationId: "888"}) {
				clientMutationId
				todo {
				  id
				  text
				  complete
				}
			}
		  }`
		var resp struct {
			RenameTodo model.RenameTodoPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
	})

	t.Run("RemoveTodo", func(t *testing.T) {
		request := `mutation {
			removeTodo(input: {id:"VG9kbzoy", clientMutationId: "87765", userId:"me@gmail.com"}) {
				deletedTodoId
				user {
				  totalCount
				  completedCount
				}
				clientMutationId
			  }
		  }`
		var resp struct {
			RemoveTodo model.RemoveTodoPayload
		}
		c.MustPost(request, &resp)
		// dump(resp)
		cupaloy.SnapshotT(t, resp)
	})

}

func dump(a interface{}) {
	b, err := json.MarshalIndent(a, "", "  ")
	log.Println(string(b), err)
}

func requireUser(t *testing.T, User model.User) {
	require.Equal(t, "VXNlcjptZUBnbWFpbC5jb20=", User.ID)
	require.Equal(t, "me@gmail.com", User.UserID)
	require.Equal(t, 1, User.CompletedCount)
	require.Equal(t, 2, User.TotalCount)

	for i, Edge := range User.Todos.Edges {
		// fmt.Printf("### %d, %#v", i, Edge.Node)
		if i == 0 {
			require.Equal(t, "Taste JavaScript", Edge.Node.Text)
			require.Equal(t, true, Edge.Node.Complete)
			require.Equal(t, "", Edge.Node.ID)
		} else if i == 1 {
			require.Equal(t, "Buy a unicorn", Edge.Node.Text)
			require.Equal(t, false, Edge.Node.Complete)
			require.Equal(t, "", Edge.Node.ID)
		}
	}
}
