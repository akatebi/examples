module bitbucket.org/akatebi/examples/todo

go 1.16

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/bradleyjkemp/cupaloy v2.3.0+incompatible
	github.com/graphql-go/graphql v0.7.9 // indirect
	github.com/graphql-go/relay v0.0.0-20171208134043-54350098cfe5
	github.com/rs/cors v1.6.0
	github.com/stretchr/testify v1.4.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
