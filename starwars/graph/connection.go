package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/akatebi/examples/starwars/graph/model"
)

func (r *Resolver) resolveFriendConnection(ctx context.Context, ids []model.Character, first *int, after *string) (*model.FriendsConnection, error) {
	from, to, err := calcRange(ids, first, after)
	if err != nil {
		return nil, err
	}
	return &model.FriendsConnection{
		TotalCount: to - from,
		Friends:    ids,
		PageInfo: &model.PageInfo{
			StartCursor: encodeCursor(from),
			EndCursor:   encodeCursor(to - 1),
			HasNextPage: to < len(ids),
		},
		Edges: edges(ids, from, to),
	}, nil
}

func calcRange(ids []model.Character, first *int, after *string) (int, int, error) {
	from := 0
	if after != nil {
		b, err := base64.StdEncoding.DecodeString(*after)
		if err != nil {
			return 0, 0, err
		}
		i, err := strconv.Atoi(strings.TrimPrefix(string(b), "cursor"))
		if err != nil {
			return 0, 0, err
		}
		from = i
	}
	to := len(ids)
	if first != nil {
		to = from + *first
		if to > len(ids) {
			to = len(ids)
		}
	}
	return from, to, nil
}

func edges(friends []model.Character, from int, to int) []*model.FriendsEdge {
	edges := make([]*model.FriendsEdge, to-from)
	for i := range edges {
		edges[i] = &model.FriendsEdge{
			Cursor: encodeCursor(from + i),
			Node:   friends[from+i],
		}
	}
	return edges
}

func encodeCursor(i int) string {
	return string(base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("cursor%d", i+1))))
}
