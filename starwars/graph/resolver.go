package graph

import (
	"bitbucket.org/akatebi/examples/starwars/graph/model"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

// Resolver ...
type Resolver struct {
	humans    map[string]*model.Human
	droids    map[string]*model.Droid
	starships map[string]*model.Starship
	reviews   map[model.Episode][]*model.Review
}

func floatPointer(v float64) *float64 {
	return &v
}

func stringPointer(s string) *string {
	return &s
}

var humans = []*model.Human{
	{
		ID:        "1000",
		Name:      "Luke Skywalker",
		Friends:   []model.Character{},
		AppearsIn: []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		Height:    1.72,
		Mass:      floatPointer(77),
		Starships: []*model.Starship{},
	},
	{
		ID:        "1001",
		Name:      "Darth Vader",
		Friends:   []model.Character{},
		AppearsIn: []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		Height:    2.02,
		Mass:      floatPointer(136),
		Starships: []*model.Starship{},
	},
	{
		ID:        "1002",
		Name:      "Han Solo",
		Friends:   []model.Character{},
		AppearsIn: []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		Height:    1.8,
		Mass:      floatPointer(80),
		Starships: []*model.Starship{},
	},
	{
		ID:        "1003",
		Name:      "Leia Organa",
		Friends:   []model.Character{},
		AppearsIn: []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		Height:    1.5,
		Mass:      floatPointer(49),
	},
	{
		ID:        "1004",
		Name:      "Wilhuff Tarkin",
		Friends:   []model.Character{},
		AppearsIn: []model.Episode{model.EpisodeNewhope},
		Height:    1.8,
		Mass:      floatPointer(0),
	},
}

var droids = []*model.Droid{
	{
		ID:              "2000",
		Name:            "C-3PO",
		Friends:         []model.Character{},
		AppearsIn:       []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		PrimaryFunction: stringPointer("Protocol"),
	},
	{
		ID:              "2001",
		Name:            "R2-D2",
		Friends:         []model.Character{},
		AppearsIn:       []model.Episode{model.EpisodeNewhope, model.EpisodeEmpire, model.EpisodeJedi},
		PrimaryFunction: stringPointer("Astromech"),
	},
}

var starships = []*model.Starship{
	{
		ID:   "3000",
		Name: "Millennium Falcon",
		History: [][]int{
			{1, 2},
			{4, 5},
			{1, 2},
			{3, 2},
		},
		Length: 34.37,
	},
	{
		ID:   "3001",
		Name: "X-Wing",
		History: [][]int{
			{6, 4},
			{3, 2},
			{2, 3},
			{5, 1},
		},
		Length: 12.5,
	},
	{
		ID:   "3002",
		Name: "TIE Advanced x1",
		History: [][]int{
			{3, 2},
			{7, 2},
			{6, 4},
			{3, 2},
		},
		Length: 9.2,
	},
	{
		ID:   "3003",
		Name: "Imperial shuttle",
		History: [][]int{
			{1, 7},
			{3, 5},
			{5, 3},
			{7, 1},
		},
		Length: 20,
	},
}

var humanData = make(map[string]*model.Human)
var droidData = make(map[string]*model.Droid)
var starshipData = make(map[string]*model.Starship)
var reviewsData = make(map[model.Episode][]*model.Review)

func init() {
	for _, h := range humans {
		humanData[h.ID] = h
	}
	for _, d := range droids {
		droidData[d.ID] = d
	}
	for _, s := range starships {
		starshipData[s.ID] = s
	}
}

// Initialize ...
func Initialize() *Resolver {
	r := Resolver{}
	r.starships = starshipData
	r.humans = humanData
	r.droids = droidData
	r.reviews = reviewsData

	for key, h := range humanData {
		switch key {
		case "1000":
			h.Friends = append(h.Friends, humanData["1002"], humanData["1003"], droidData["2000"], droidData["2001"])
			h.Starships = append(h.Starships, starshipData["3001"], starshipData["3003"])
		case "1001":
			h.Friends = append(h.Friends, humanData["1004"])
			h.Starships = append(h.Starships, starshipData["3002"])
		case "1002":
			h.Friends = append(h.Friends, humanData["1000"], humanData["1003"], droidData["2001"])
			h.Starships = append(h.Starships, starshipData["3000"], starshipData["3003"])
		case "1003":
			h.Friends = append(h.Friends, humanData["1000"], humanData["1002"], droidData["2000"], droidData["2001"])
		case "1004":
			h.Friends = append(h.Friends, humanData["1001"])
		}
	}

	for key, d := range droidData {
		switch key {
		case "2000":
			d.Friends = append(d.Friends, humanData["1000"], humanData["1002"], humanData["1003"], droidData["2001"])
		case "2001":
			d.Friends = append(d.Friends, humanData["1000"], humanData["1002"], humanData["1003"])
		}
	}

	return &r
}
