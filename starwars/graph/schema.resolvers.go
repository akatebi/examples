package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/akatebi/examples/starwars/graph/generated"
	"bitbucket.org/akatebi/examples/starwars/graph/model"
)

func (r *droidResolver) FriendsConnection(ctx context.Context, obj *model.Droid, first *int, after *string) (*model.FriendsConnection, error) {
	return r.resolveFriendConnection(ctx, obj.Friends, first, after)
}

func (r *humanResolver) Height(ctx context.Context, obj *model.Human, unit *model.LengthUnit) (float64, error) {
	height := obj.Height
	if *unit == model.LengthUnitFoot {
		height *= 3.28084
	}
	return height, nil
}

func (r *humanResolver) FriendsConnection(ctx context.Context, obj *model.Human, first *int, after *string) (*model.FriendsConnection, error) {
	return r.resolveFriendConnection(ctx, obj.Friends, first, after)
}

func (r *mutationResolver) CreateReview(ctx context.Context, episode model.Episode, review model.ReviewInput) (*model.Review, error) {
	now := time.Now()
	review.Time = &now
	time.Sleep(1 * time.Second) // To satisfy go test
	newReview := model.Review(review)
	r.reviews[episode] = append(r.reviews[episode], &newReview)
	return &newReview, nil
}

func (r *queryResolver) Hero(ctx context.Context, episode *model.Episode) (model.Character, error) {
	if *episode == model.EpisodeEmpire {
		if h, ok := r.humans["1000"]; ok {
			return h, nil
		}
	} else if *episode == model.EpisodeJedi {
		if d, ok := r.droids["2000"]; ok {
			return d, nil
		}
	}
	if d, ok := r.droids["2001"]; ok {
		return d, nil
	}
	return nil, fmt.Errorf("hero not found")
}

func (r *queryResolver) Reviews(ctx context.Context, episode model.Episode, since *time.Time) ([]*model.Review, error) {
	return r.reviews[episode], nil
}

func (r *queryResolver) Search(ctx context.Context, text string) ([]model.SearchResult, error) {
	var l []model.SearchResult
	for _, h := range r.humans {
		if strings.Contains(h.Name, text) {
			l = append(l, h)
		}
	}
	for _, d := range r.droids {
		if strings.Contains(d.Name, text) {
			l = append(l, d)
		}
	}
	for _, s := range r.starships {
		if strings.Contains(s.Name, text) {
			l = append(l, s)
		}
	}
	return l, nil
}

func (r *queryResolver) Character(ctx context.Context, id string) (model.Character, error) {
	if h, ok := r.humans[id]; ok {
		return h, nil
	}
	if d, ok := r.droids[id]; ok {
		return d, nil
	}
	return nil, nil
}

func (r *queryResolver) Droid(ctx context.Context, id string) (*model.Droid, error) {
	if d := r.droids[id]; d != nil {
		return d, nil
	}
	return nil, fmt.Errorf("droid not found")
}

func (r *queryResolver) Human(ctx context.Context, id string) (*model.Human, error) {
	if h := r.humans[id]; h != nil {
		return h, nil
	}
	return nil, fmt.Errorf("human not found")
}

func (r *queryResolver) Starship(ctx context.Context, id string) (*model.Starship, error) {
	if s := r.starships[id]; s != nil {
		return s, nil
	}
	return nil, fmt.Errorf("starship not found")
}

func (r *starshipResolver) Length(ctx context.Context, obj *model.Starship, unit *model.LengthUnit) (float64, error) {
	length := obj.Length
	if *unit == model.LengthUnitFoot {
		length *= 3.28084
	}
	return length, nil
}

// Droid returns generated.DroidResolver implementation.
func (r *Resolver) Droid() generated.DroidResolver { return &droidResolver{r} }

// Human returns generated.HumanResolver implementation.
func (r *Resolver) Human() generated.HumanResolver { return &humanResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Starship returns generated.StarshipResolver implementation.
func (r *Resolver) Starship() generated.StarshipResolver { return &starshipResolver{r} }

type droidResolver struct{ *Resolver }
type humanResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type starshipResolver struct{ *Resolver }
