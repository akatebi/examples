# The query type, represents all of the entry points into our object graph
type Query {
    hero(episode: Episode = NEWHOPE): Character
    reviews(episode: Episode!, since: Time): [Review!]!
    search(text: String!): [SearchResult!]!
    character(id: ID!): Character
    droid(id: ID!): Droid
    human(id: ID!): Human
    starship(id: ID!): Starship
}
# The mutation type, represents all updates we can make to our data
type Mutation {
    createReview(episode: Episode!, review: ReviewInput!): Review
}
# The episodes in the Star Wars trilogy
enum Episode {
    NEWHOPE
    EMPIRE
    JEDI
}
# A character from the Star Wars universe
interface Character {
    id: ID!
    name: String!
    friends: [Character!]
    friendsConnection(first: Int, after: ID): FriendsConnection!
    appearsIn: [Episode!]!
}
# Units of height
enum LengthUnit {
    METER
    FOOT
}
# A humanoid creature from the Star Wars universe
type Human implements Character {
    id: ID!
    name: String!
    height(unit: LengthUnit = METER): Float!
    mass: Float
    friends: [Character!]
    friendsConnection(first: Int, after: ID): FriendsConnection!
    appearsIn: [Episode!]!
    starships: [Starship!]
}
# An autonomous mechanical character in the Star Wars universe
type Droid implements Character {
    id: ID!
    name: String!
    friends: [Character!]
    friendsConnection(first: Int, after: ID): FriendsConnection!
    appearsIn: [Episode!]!
    primaryFunction: String
}
# A connection object for a character's friends
type FriendsConnection {
    totalCount: Int!
    edges: [FriendsEdge!]
    friends: [Character!]
    pageInfo: PageInfo!
}
# An edge object for a character's friends
type FriendsEdge {
    cursor: ID!
    node: Character
}
# Information for paginating this connection
type PageInfo {
    startCursor: ID!
    endCursor: ID!
    hasNextPage: Boolean!
}
# Represents a review for a movie
type Review {
    stars: Int!
    commentary: String
    time: Time
}
# The input object sent when someone is creating a new review
input ReviewInput {
    stars: Int!
    commentary: String
    time: Time
}
type Starship {
    id: ID!
    name: String!
    length(unit: LengthUnit = METER): Float!
    history: [[Int!]!]!
}
union SearchResult = Human | Droid | Starship
scalar Time
