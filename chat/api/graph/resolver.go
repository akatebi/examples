package graph

import (
	"context"
	"math/rand"
	"sync"

	"bitbucket.org/akatebi/examples/chat/graph/generated"
	"bitbucket.org/akatebi/examples/chat/graph/model"
	"github.com/99designs/gqlgen/graphql"
)

type ckey string

// Resolver ...
type Resolver struct {
	Rooms        map[string]*model.Chatroom
	sync.RWMutex // nolint: structcheck
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randString(n int) string {
	b := make([]rune, n)
	for i := range b {
		max := len(letterRunes)
		j := rand.Intn(max)
		b[i] = letterRunes[j]
	}
	return string(b)
}

func getUsername(ctx context.Context) string {
	if username, ok := ctx.Value(ckey("username")).(string); ok {
		return username
	}
	return ""
}

func (r *Resolver) getRoom(roomName string) *model.Chatroom {
	r.Lock()
	room := r.Rooms[roomName]
	if room == nil {
		room = &model.Chatroom{
			Name:      roomName,
			Observers: map[string]model.Observer{},
		}
		r.Rooms[roomName] = room
	}
	r.Unlock()
	return room
}

// New ...
func New() generated.Config {
	return generated.Config{
		Resolvers: &Resolver{
			Rooms: map[string]*model.Chatroom{},
		},
		Directives: generated.DirectiveRoot{
			User: func(ctx context.Context, obj interface{}, next graphql.Resolver, username string) (res interface{}, err error) {
				return next(context.WithValue(ctx, ckey("username"), username))
			},
		},
	}
}
