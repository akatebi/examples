package model

// Observer ...
type Observer struct {
	Username string
	Message  chan *Message
}

// Chatroom ...
type Chatroom struct {
	Name      string
	Messages  []Message
	Observers map[string]Observer
}
