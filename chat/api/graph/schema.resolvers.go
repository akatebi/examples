package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"time"

	"bitbucket.org/akatebi/examples/chat/graph/generated"
	"bitbucket.org/akatebi/examples/chat/graph/model"
)

func (r *mutationResolver) Post(ctx context.Context, text string, username string, roomName string) (*model.Message, error) {
	room := r.getRoom(roomName)
	message := model.Message{
		ID:        randString(8),
		CreatedAt: time.Now(),
		Text:      text,
		CreatedBy: username,
	}
	room.Messages = append(room.Messages, message)
	r.RLock()
	for _, observer := range room.Observers {
		if observer.Username == message.CreatedBy || observer.Username == "" {
			observer.Message <- &message
		}
	}
	r.RUnlock()
	return &message, nil
}

func (r *queryResolver) Room(ctx context.Context, name string) (*model.Chatroom, error) {
	room := r.getRoom(name)
	return room, nil
}

func (r *subscriptionResolver) MessageAdded(ctx context.Context, roomName string) (<-chan *model.Message, error) {
	room := r.getRoom(roomName)
	id := randString(8)
	go func() {
		<-ctx.Done()
		r.Lock()
		delete(room.Observers, id)
		r.Unlock()
	}()
	events := make(chan *model.Message, 1)
	r.Lock()
	room.Observers[id] = model.Observer{
		Username: getUsername(ctx),
		Message:  events,
	}
	r.Unlock()
	return events, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
