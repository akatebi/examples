import React from "react";
import { useRecoilValue, atom, selector } from "recoil";

export const textState = atom({
    key: 'textState',
    default: '',
});


const charCountState = selector({
    key: 'charCountState', // unique ID (with respect to other atoms/selectors)
    get: ({get}) => {
      const text = get(textState);
      return text.length;
    },
  });

export function CharacterCount() {
    const count = useRecoilValue(charCountState);
  
    return <>Character Count: {count}</>;
  }