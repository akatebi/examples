import React from 'react';
import { RecoilRoot } from 'recoil';
import './App.css';
import { CharacterCounter } from "./CharacterCounter";

function App() {
  return (
      <RecoilRoot>
        <CharacterCounter />
      </RecoilRoot>
    );
}

export default App;
