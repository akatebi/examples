import React, { useState } from "react";

import { useSetRecoilState } from "recoil";

import { todoListState } from "./todoState";

export function TodoItemCreator() {
  //
  const [inputValue, setInputValue] = useState("");
  const setTodoList = useSetRecoilState(todoListState);

  const addItem = () => {
    if (inputValue) {
      setTodoList((oldTodoList) => [
        ...oldTodoList,
        {
          id: getId(),
          text: inputValue,
          isComplete: false,
        },
      ]);
      setInputValue("");
    }
  };

  const onKeyPress = ({ key }) => {
    if (key === "Enter") {
      addItem();
    }
  };

  const onChange = ({ target: { value } }) => {
    setInputValue(value);
  };

  return (
    <>
      <input
        type="text"
        value={inputValue}
        onChange={onChange}
        onKeyPress={onKeyPress}
      />
      <button onClick={addItem}>Add</button>
    </>
  );
}

// utility for creating unique Id
let id = 0;
function getId() {
  return id++;
}
