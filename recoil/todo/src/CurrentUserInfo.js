import React from "react";
import { atom, selector, useRecoilValue } from "recoil";

const tableOfUsers = [
  {
    name: "me"
  }
];

const currentUserIDState = atom({
  key: "CurrentUserID",
  default: 0,
});

const currentUserNameState = selector({
  key: "CurrentUserName",
  get: ({ get }) => {
    return tableOfUsers[get(currentUserIDState)].name;
  },
});

export function CurrentUserInfo() {
  const userName = useRecoilValue(currentUserNameState);
  return <div>{userName}</div>;
}
