#!/bin/bash

port=$1

if [ -z $port ];then
  port=8080
fi

curl localhost:$port/query  \
    -F operations='{ "query": "mutation ($file: Upload!) { singleUpload(file: $file) { id, name, content } }", "variables": { "file": null } }'   \
    -F map='{ "0": ["variables.file"] }'  \
    -F 0=@./go.sum | jq

# curl localhost:$port/query \
#   -F operations='{ "query": "mutation($files: [Upload!]!) { multipleUpload(files: $files) { id, name, content } }", "variables": { "files": [null, null] } }' \
#   -F map='{ "0": ["variables.files.0"], "1": ["variables.files.1"] }' \
#   -F 0=@./testfiles/b.txt \
#   -F 1=@./testfiles/c.txt | jq

# printf "\n"


# fileupload example

# Single file
curl localhost:$port/query \
  -F operations='{ "query": "mutation ($file: Upload!) { singleUpload(file: $file) { id, name, content } }", "variables": { "file": null } }' \
  -F map='{ "0": ["variables.file"] }' \
  -F 0=@./testfiles/a.txt | jq

# Single file with payload
curl localhost:$port/query \
  -F operations='{ "query": "mutation ($req: UploadFile!) { singleUploadWithPayload(req: $req) { id, name, content } }", "variables": { "req": {"file": null, "id": 1 } } }' \
  -F map='{ "0": ["variables.req.file"] }' \
  -F 0=@./testfiles/a.txt | jq

# Mulitiple file
curl localhost:$port/query \
  -F operations='{ "query": "mutation($files: [Upload!]!) { multipleUpload(files: $files) { id, name, content } }", "variables": { "files": [null, null] } }' \
  -F map='{ "0": ["variables.files.0"], "1": ["variables.files.1"] }' \
  -F 0=@./testfiles/b.txt \
  -F 1=@./testfiles/c.txt | jq

# Multiple file with payload
curl localhost:$port/query \
  -F operations='{ "query": "mutation($req: [UploadFile!]!) { multipleUploadWithPayload(req: $req) { id, name, content } }", "variables": { "req": [ { "id": 1, "file": null }, { "id": 2, "file": null } ] } }' \
  -F map='{ "0": ["variables.req.0.file"], "1": ["variables.req.1.file"] }' \
  -F 0=@./testfiles/b.txt \
  -F 1=@./testfiles/c.txt | jq


