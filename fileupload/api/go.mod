module bitbucket.org/akatebi/examples/fileupload/api

go 1.14

require (
	github.com/99designs/gqlgen v0.12.2
	github.com/rs/cors v1.6.0
	github.com/stretchr/testify v1.4.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
