'use strict';

const { GraphQLSchema, printSchema } = require('graphql');
const MutationType = require('./MutationType');
const QueryType = require('./QueryType');

const schema = new GraphQLSchema({
  query: QueryType,
  mutation: MutationType,
});

// console.log(printSchema(schema));

module.exports = schema;
